/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef KEYBOARDCONTROLER_HPP
#define KEYBOARDCONTROLER_HPP
#include <iostream>
#include <vector>
#include <Controler.hpp>
#include <SFML/Graphics.hpp>

class KeyboardControler : public Controler
{
 public:
  explicit KeyboardControler();
  explicit KeyboardControler(std::vector<sf::Keyboard::Key> ctrls);
  virtual ~KeyboardControler();

  virtual bool up() override;
  virtual bool down() override;
  virtual bool left() override;
  virtual bool right() override;
  virtual bool accept() override;
  virtual bool cancel() override;
  virtual bool alternative() override;
  
 protected:
  sf::Keyboard::Key m_up;
  sf::Keyboard::Key m_down;
  sf::Keyboard::Key m_left;
  sf::Keyboard::Key m_right;
  sf::Keyboard::Key m_accept;
  sf::Keyboard::Key m_cancel;
  sf::Keyboard::Key m_alternative;

 private:
  KeyboardControler( KeyboardControler const& keyboardcontroler ) = delete;
  KeyboardControler& operator=( KeyboardControler const& keyboardcontroler ) = delete;
};

#endif
