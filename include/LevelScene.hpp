/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LEVELSCENE_HPP
#define LEVELSCENE_HPP
#include <iostream>
#include <Scene.hpp>

class GraphicalNumGrid;
class Level;
class Cursor;

enum
  {
    GRID_ELEM_W = 16
    , GRID_ELEM_H = 16
  };

class LevelScene : public Scene
{
 public:
  explicit LevelScene(Core* core);
  virtual ~LevelScene();
  
  void updateInput(size_t player);
  void update() override;
  void display(Render* render) override;
  
 protected:
  std::unique_ptr<GraphicalNumGrid> m_grid;
  std::unique_ptr<Level> m_level;
  int m_inputs_delay;
  std::vector<sf::Clock> m_inputs_clocks;
  
  Cursor* getCursor(size_t cursor);
 private:
  LevelScene( LevelScene const& levelscene ) = delete;
  LevelScene& operator=( LevelScene const& levelscene ) = delete;
};

#endif
