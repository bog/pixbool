/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <memory>
#include <SFML/Graphics.hpp>
#include <KeyboardControler.hpp>

#define UNUSED(x) ((void)(x))

class Render;
class Scene;

class Core
{
 public:
  explicit Core();
  virtual ~Core();
  
  inline sf::Vector2f getSize()
  {
    return sf::Vector2f( static_cast<float>(m_window->getSize().x)
			 , static_cast<float>(m_window->getSize().y) );
  }

  inline size_t getControlerCount() { return m_controlers.size(); }
  
  inline Controler* getControler(size_t i)
  {
    if(i >= m_controlers.size())
      {
	throw std::runtime_error("Can't get controler number "
				 + std::to_string(i));
      }
    
    return m_controlers[i].get();
  }
  
  void start();
  
  inline void loadNoScene() { changeScene(nullptr); }
  inline void loadMenuScene() { changeScene(m_menu_scene.get()); }
  inline void loadLevelScene() { changeScene(m_level_scene.get()); }
  
 protected:
  void update();
  void display();
  void gameLoop();
  void changeScene(Scene* scene);
    
  bool m_running;
  std::unique_ptr<sf::RenderWindow> m_window;
  std::unique_ptr<Render> m_render;
  
  Scene* m_current_scene;
  std::unique_ptr<Scene> m_menu_scene;
  std::unique_ptr<Scene> m_level_scene;

  std::vector< std::unique_ptr<Controler> > m_controlers;
  
 private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;
};

#endif
