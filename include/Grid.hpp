/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRID_HPP
#define GRID_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

enum class GridValue
  {
    Empty
      , Cross
      , Block
  };

class Grid
{
 public:
  explicit Grid(unsigned int width, unsigned int height);
  virtual ~Grid();

  void dump();
  void loadFromImage(sf::Image image);

  inline bool in(unsigned int i, unsigned int j){ return i<m_height && j<m_width; }
  
  inline GridValue get(unsigned int i, unsigned int j)
  {
    if( !in(i, j) ){throw std::logic_error("Can't access grid "
					     + std::to_string(i)
					     + " "
					     + std::to_string(j));}
    return m_grid[i][j];
  }  
  
  inline void set(unsigned int i, unsigned int j, GridValue value)
  {
    if( !in(i, j) ){throw std::logic_error("Can't access grid "
					     + std::to_string(i)
					     + " "
					     + std::to_string(j));}
    m_grid[i][j] = value;
  }
  
  inline sf::Vector2u getSize() { return sf::Vector2u(m_width, m_height); }
 protected:
  unsigned int const m_width;
  unsigned int const m_height;
  GridValue** m_grid;
  
 private:
  Grid( Grid const& grid ) = delete;
  Grid& operator=( Grid const& grid ) = delete;
};

#endif
