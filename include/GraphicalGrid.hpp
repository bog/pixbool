/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAPHICALGRID_HPP
#define GRAPHICALGRID_HPP
#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>

class Render;
class Level;
class Cursor;
class VisualHelper;

class GraphicalGrid
{
 public:
  explicit GraphicalGrid(Level* level, sf::FloatRect area);
  
  virtual ~GraphicalGrid();

  void update();
  void display(Render* render);

  bool in(sf::Vector2i pos);
  void cursorMove(size_t cursor, sf::Vector2i dpos);
  
  sf::Vector2u getCursorPos(size_t cursor);
  sf::Vector2u getCursorPos(Cursor* cursor);
  
  void cursorClean(size_t cursor);
  void cursorMark(size_t cursor);
  void cursorCross(size_t cursor);
  
  void addCursor(sf::Vector2i pos, sf::Color color);
  inline void addCursor(sf::Color color) { addCursor({0, 0}, color); }
  
  inline float getBlocWidth() const { return m_bloc_width; }
  inline float getBlocHeight() const { return m_bloc_height; }
  inline sf::FloatRect getArea() const { return m_area; }
    
  inline Cursor* getCursor(size_t cursor) const
  {
    if( cursor >= m_cursors.size() )
      {
	throw std::logic_error("Cursor number "
			       + std::to_string(cursor)
			       + " does not exists.");
      }

    return m_cursors[cursor].get();
  }

 protected:
  Level* m_level;

  sf::FloatRect m_area;
  
  float const m_bloc_width;
  float const m_bloc_height;
  
  unsigned int m_division;
  float m_grid_thickness;
  
  sf::RectangleShape m_grid_line;
  sf::RectangleShape m_grid_element;
  sf::RectangleShape m_grid_bg;
  
  sf::Color m_grid_line_primary_color;
  sf::Color m_grid_line_secondary_color;
  sf::Color m_grid_bg_color;

  std::vector< std::unique_ptr<Cursor> > m_cursors;
  std::vector< std::unique_ptr<VisualHelper> > m_visual_helpers;

  bool m_drawLines;
    
  void displayBlocs(Render* render);
  void displayLines(Render* render);  
  
 private:
  GraphicalGrid( GraphicalGrid const& graphicalgrid ) = delete;
  GraphicalGrid& operator=( GraphicalGrid const& graphicalgrid ) = delete;
};

#endif
