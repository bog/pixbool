/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef LEVEL_HPP
#define LEVEL_HPP
#include <iostream>
#include <vector>
#include <Grid.hpp>

typedef std::vector<unsigned int> Numbers;

class Level
{
 public:
  explicit Level(std::string grid_path);
  virtual ~Level();

  void setRef(unsigned int i, unsigned int j, GridValue value);
  void setUsr(unsigned int i, unsigned int j, GridValue value);
  bool isComplete();

  inline GridValue getRef(unsigned int i, unsigned int j) { return m_reference_grid->get(i, j); }
  inline GridValue getUsr(unsigned int i, unsigned int j) { return m_user_grid->get(i, j); }

  inline sf::Vector2u getSize() { return m_image.getSize(); }

  inline sf::Color getColor(unsigned int i, unsigned int j)
  {
    return m_image.getPixel(j , i);
  }

  inline std::vector<unsigned int> getXNumbers(size_t i) { return m_x_numbers[i]; }
  inline std::vector<unsigned int> getYNumbers(size_t i) { return m_y_numbers[i]; }
  
 protected:
  std::unique_ptr<Grid> m_user_grid;
  std::unique_ptr<Grid> m_reference_grid;
  sf::Image m_image;
  std::vector<Numbers> m_x_numbers;
  std::vector<Numbers> m_y_numbers;

  unsigned int m_total_empty;
  unsigned int m_total_block;
  unsigned int m_empty_matched;
  unsigned int m_block_matched;
  
  void initNumbers();
  void initMatched();
  void updateMatched(unsigned int i, unsigned int j, GridValue value);
 private:
  Level( Level const& level ) = delete;
  Level& operator=( Level const& level ) = delete;
};

#endif
