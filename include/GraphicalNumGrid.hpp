/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef GRAPHICALNUMGRID_HPP
#define GRAPHICALNUMGRID_HPP
#include <iostream>
#include <SFML/Graphics.hpp>
#include <memory.h>

class Render;
class Level;
class GraphicalGrid;

class GraphicalNumGrid
{
 public:
  explicit GraphicalNumGrid(Level* level, sf::FloatRect rect);
  virtual ~GraphicalNumGrid();

  void update();
  void display(Render* render);
  inline GraphicalGrid* getInnerGrid() { return m_grid.get(); }
  
 protected:
  Level* m_level;
  sf::FloatRect m_area;
  std::unique_ptr<GraphicalGrid> m_grid;

  float m_number_x_offset;
  float m_number_y_offset;
  
  sf::Text m_number;
  sf::Font m_number_font;
  
  void displayNumbers	(Render* render);
  void displayXNumbers	(Render* render);
  void displayYNumbers	(Render* render);
  
 private:
  GraphicalNumGrid( GraphicalNumGrid const& graphicalnumgrid ) = delete;
  GraphicalNumGrid& operator=( GraphicalNumGrid const& graphicalnumgrid ) = delete;
};

#endif
