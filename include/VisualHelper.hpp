/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef VISUALHELPER_HPP
#define VISUALHELPER_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

class Cursor;
class Render;
class GraphicalGrid;

class VisualHelper
{
 public:
  explicit VisualHelper(GraphicalGrid* grid, Cursor* cursor);
  virtual ~VisualHelper();

  void update();
  void display(Render* render);
  
 protected:
  GraphicalGrid* m_grid;
  Cursor* m_cursor;

  sf::Color m_color;
  unsigned short int m_opacity;
  
  sf::RectangleShape m_vertical;
  sf::RectangleShape m_horizontal;
  
 private:
  VisualHelper( VisualHelper const& visualhelper ) = delete;
  VisualHelper& operator=( VisualHelper const& visualhelper ) = delete;
};

#endif
