/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef RENDER_HPP
#define RENDER_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

class Render
{
 public:
  explicit Render(sf::RenderWindow* window);
  virtual ~Render();

  void draw(sf::Drawable& drawable);
  void view(sf::View v);
  inline sf::View view() { return m_window->getView(); }
  
 protected:
  sf::RenderWindow* m_window;
  
 private:
  Render( Render const& render ) = delete;
  Render& operator=( Render const& render ) = delete;
};

#endif
