/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CONTROLER_HPP
#define CONTROLER_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

class Controler
{
 public:
  explicit Controler();
  virtual ~Controler();

  virtual bool up() = 0;
  virtual bool down() = 0;
  virtual bool left() = 0;
  virtual bool right() = 0;
  virtual bool accept() = 0;
  virtual bool cancel() = 0;
  virtual bool alternative() = 0;
  
 protected:

 private:
  Controler( Controler const& controler ) = delete;
  Controler& operator=( Controler const& controler ) = delete;
};

#endif
