/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#ifndef CURSOR_HPP
#define CURSOR_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

class Render;

class Cursor
{
 public:
  explicit Cursor(sf::FloatRect rect, sf::Color color);
  virtual ~Cursor();

  void update();
  void display(Render* render);

  void move(sf::Vector2f dpos);
  
  inline sf::FloatRect getRect() { return m_rect; }
  inline sf::Color getColor() { return m_color; }
  
 protected:
  sf::FloatRect m_rect;
  sf::Color m_color;
  sf::RectangleShape m_rs;
  
 private:
  Cursor( Cursor const& cursor ) = delete;
  Cursor& operator=( Cursor const& cursor ) = delete;
};

#endif
