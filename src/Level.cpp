/* Copyright (C) 2016 bog

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Level.hpp>

Level::Level(std::string grid_path)
  : m_total_empty(0)
  , m_total_block(0)
  , m_empty_matched(0)
  , m_block_matched(0)
{
  m_image.loadFromFile(grid_path);
  
  m_user_grid = std::make_unique<Grid>(m_image.getSize().x
				       , m_image.getSize().y);
  m_reference_grid = std::make_unique<Grid>(m_image.getSize().x
					    , m_image.getSize().y);
  m_reference_grid->loadFromImage(m_image);

  initNumbers();
  initMatched();
}

void Level::initNumbers()
{
  for(size_t j=0; j<m_user_grid->getSize().x; j++)
    {
      m_x_numbers.push_back(Numbers());
      
      int count = 0;
      
      for(size_t k=0; k<m_user_grid->getSize().y; k++)
	{
	  if(m_image.getPixel(j, k) != sf::Color::White)
	    {
	      count++;
	    }
	  
	  if( (k == m_user_grid->getSize().y -1
	       || m_image.getPixel(j, k) == sf::Color::White)
	      && count != 0 )
	    {
	      m_x_numbers[j].push_back(count);
	      count = 0;
	    }
	}
      
      if( m_x_numbers[j].empty() ) { m_x_numbers[j].push_back(0); }
    }
  
  for(size_t i=0; i<m_user_grid->getSize().y; i++)
    {
      m_y_numbers.push_back(Numbers());

      int count = 0;
      
      for(size_t k=0; k<m_user_grid->getSize().x; k++)
	{
	  if(m_image.getPixel(k, i) != sf::Color::White)
	    {
	      count++;
	    }
	  
	  if( (k == m_user_grid->getSize().x -1
	       || m_image.getPixel(k, i) == sf::Color::White)
	      && count != 0 )
	    {
	      m_y_numbers[i].push_back(count);
	      count = 0;
	    }
	}
      
      if( m_y_numbers[i].empty() ) { m_y_numbers[i].push_back(0); }
    }
}

void Level::initMatched()
{
  // initial count
  for(size_t i=0; i<m_user_grid->getSize().y; i++)
    {
      for(size_t j=0; j<m_user_grid->getSize().x; j++)
	{
	  // count empty
	  if(m_reference_grid->get(i, j) == GridValue::Empty)
	    {
	      m_total_empty++;
	    }

	  // count block
	  if(m_reference_grid->get(i, j) == GridValue::Block)
	    {
	      m_total_block++;
	    }
	}
    }
  
  m_empty_matched = m_total_empty;
}

bool Level::isComplete()
{
  return (m_block_matched == m_total_block
	  && m_empty_matched == m_total_empty);
}

void Level::setRef(unsigned int i, unsigned int j, GridValue value)
{
  m_reference_grid->set(i, j, value);
}

void Level::updateMatched(unsigned int i, unsigned int j, GridValue value)
{
  GridValue ref_value = m_reference_grid->get(i, j);
  GridValue old_value = m_user_grid->get(i, j);

  if(old_value == GridValue::Block)
    // we are removing a block
    {
      if(ref_value == GridValue::Block)
	// removing a good block
	{
	  if(m_block_matched > 0) { m_block_matched--; }
	}
      else
	//removing a bad block
	{
	  m_empty_matched++;
	}
    }
  else
    // we are removing a cross
    // or an empty place
    {
      if(value == GridValue::Block)
	// and we add a block
	{	  	  
	  if(ref_value == GridValue::Block)
	    // the block is good
	    {
	      m_block_matched++;
	    }
	  else
	    // the block is bad
	    {
	      if(m_empty_matched > 0) { m_empty_matched--; }
	    }
	}
      else if(value == GridValue::Cross)
	// and we add a cross
	{
	  // nothing append
	}
    }
}

void Level::setUsr(unsigned int i, unsigned int j, GridValue value)
{
  if( m_user_grid->get(i, j) == value ) { return; }

  updateMatched(i, j, value);
  
  m_user_grid->set(i, j, value);
}

Level::~Level()
{
  
}
