/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Core.hpp>
#include <Config.hpp>

#include <Scene.hpp>
#include <MenuScene.hpp>
#include <LevelScene.hpp>

Core::Core()
  : m_current_scene(nullptr)
{
  sf::VideoMode mode = sf::VideoMode::getDesktopMode();
  mode.width = 0.9 * mode.width;
  mode.height = 0.9 * mode.height;
  
  m_window = std::make_unique<sf::RenderWindow>(mode
						, PROJECT_STRING
						, sf::Style::Titlebar | sf::Style::Close);
  
  m_render = std::make_unique<Render>( m_window.get() );

  m_menu_scene = std::make_unique<MenuScene>(this);
  m_level_scene = std::make_unique<LevelScene>(this);

  m_controlers.push_back(std::unique_ptr<Controler>( new KeyboardControler({
  	  sf::Keyboard::Z
  	    , sf::Keyboard::S
  	    , sf::Keyboard::Q
  	    , sf::Keyboard::D
  	    , sf::Keyboard::F
  	    , sf::Keyboard::H
  	    , sf::Keyboard::G
  	    }) ));

  m_controlers.push_back(std::unique_ptr<Controler>( new KeyboardControler({
	  sf::Keyboard::Up
	    , sf::Keyboard::Down
	    , sf::Keyboard::Left
	    , sf::Keyboard::Right
	    , sf::Keyboard::Add
	    , sf::Keyboard::Subtract
	    , sf::Keyboard::Multiply
	    }) ));

  changeScene( m_level_scene.get() );
}

void Core::changeScene(Scene* scene)
{  
  m_current_scene = scene;
}

void Core::start()
{
  gameLoop();
}

void Core::update()
{
  if(m_current_scene)
    {
      m_current_scene->update();
    }
}

void Core::display()
{
  if(m_current_scene)
    {
      m_current_scene->display( m_render.get() );
    }
}

void Core::gameLoop()
{
  sf::Event event;
  
  while(m_running)
    {
      while( m_window->pollEvent(event) )
	{
	  if(event.type == sf::Event::Closed)
	    {
	      m_running = false;
	      m_window->close();
	    }
	}
      
      update();
      
      m_window->clear( sf::Color(4, 139, 154) );
      display();
      m_window->display();
    }
}

Core::~Core()
{
  
}
