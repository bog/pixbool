/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or


    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <GraphicalGrid.hpp>
#include <Render.hpp>
#include <Level.hpp>
#include <Cursor.hpp>
#include <VisualHelper.hpp>
#include <cmath>

GraphicalGrid::GraphicalGrid(Level* level, sf::FloatRect area)
  : m_level			( level )
  , m_area			( area )
  , m_bloc_width		( m_area.width/static_cast<float>(level->getSize().x) )
  , m_bloc_height		( m_area.height/static_cast<float>(level->getSize().y) )
  , m_division			( 5 )
  , m_grid_thickness		( 1 )
  , m_grid_line_primary_color	( sf::Color(200, 200, 200) )
  , m_grid_line_secondary_color	( sf::Color(128, 64, 64) )
  , m_grid_bg_color		( sf::Color(200, 200, 200) )
  , m_drawLines			( true )
{  
  m_grid_element.setSize({m_bloc_width, m_bloc_height});
  
  m_grid_bg = sf::RectangleShape({m_area.width, m_area.height});
  m_grid_bg.setFillColor(m_grid_bg_color);
  m_grid_bg.setPosition({m_area.left, m_area.top});
}

void GraphicalGrid::update()
{
  for(std::unique_ptr<Cursor>& cursor : m_cursors)
    {
      cursor->update();
    }

  for(std::unique_ptr<VisualHelper>& helper : m_visual_helpers)
    {
      helper->update();
    }
}

bool GraphicalGrid::in(sf::Vector2i pos)
{
  return (pos.x >= 0
	  && pos.x < static_cast<int>(m_level->getSize().x)
	  && pos.y >= 0
	  && pos.y < static_cast<int>(m_level->getSize().y) );
}

void GraphicalGrid::addCursor(sf::Vector2i pos, sf::Color color)
{
  m_cursors.push_back( std::make_unique<Cursor>( sf::FloatRect( m_area.left + pos.x * m_bloc_width
								, m_area.top + pos.y * m_bloc_height
								, m_bloc_width
								, m_bloc_height)
						 , color) );
  
  m_visual_helpers.push_back( std::make_unique<VisualHelper>( this, m_cursors.back().get() ) );
}

void GraphicalGrid::cursorClean(size_t cursor)
{
  m_level->setUsr(getCursorPos(cursor).y, getCursorPos(cursor).x, GridValue::Empty);
}

void GraphicalGrid::cursorMark(size_t cursor)
{
  m_level->setUsr(getCursorPos(cursor).y, getCursorPos(cursor).x, GridValue::Block);
}

void GraphicalGrid::cursorCross(size_t cursor)
{
  m_level->setUsr(getCursorPos(cursor).y, getCursorPos(cursor).x, GridValue::Cross);
}


void GraphicalGrid::cursorMove(size_t cursor, sf::Vector2i dpos)
{
  sf::Vector2i pos = sf::Vector2i( static_cast<int>(getCursorPos(cursor).x)
				   , static_cast<int>(getCursorPos(cursor).y) );
  
  if( !in( sf::Vector2i(pos.x + dpos.x, pos.y + dpos.y) ) )
    {
      throw std::runtime_error("Can't move the cursor of ("
			       + std::to_string(dpos.x)
			       + ", "
			       + std::to_string(dpos.y)
			       + ") - ie at the position ("
			       + std::to_string(pos.x + dpos.x)
			       + ", "
			       + std::to_string(pos.y + dpos.y)
			       + ").");
    }

  getCursor(cursor)->move(sf::Vector2f(dpos.x * m_bloc_width
			      , dpos.y * m_bloc_height));
}

sf::Vector2u GraphicalGrid::getCursorPos(Cursor* cursor)
{
  sf::Vector2u pos;
  pos.x = round((cursor->getRect().left - m_area.left)/m_bloc_width);
  pos.y = round((cursor->getRect().top - m_area.top)/m_bloc_height);
  return pos;
}

sf::Vector2u GraphicalGrid::getCursorPos(size_t cursor)
{
  return getCursorPos( getCursor(cursor) );
}

void GraphicalGrid::displayLines(Render* render)
{
  m_grid_line.setSize({m_area.width, m_grid_thickness});
  
  for(size_t i=1; i<m_level->getSize().y; i++)
    {
      m_grid_line.setPosition({m_area.left, static_cast<float>(i) * m_bloc_height + m_area.top});
      
      if(i%(m_division)!=0){ m_grid_line.setFillColor(m_grid_line_primary_color); }
      else { m_grid_line.setFillColor(m_grid_line_secondary_color); }
      
      render->draw(m_grid_line);
    }

  m_grid_line.setSize({m_grid_thickness, static_cast<float>(m_level->getSize().y) * m_bloc_height});
  
  for(size_t j=1; j<m_level->getSize().x; j++)
    {
      m_grid_line.setPosition({static_cast<float>(j) * m_bloc_width + m_area.left, m_area.top});

      if(j%(m_division)!=0){ m_grid_line.setFillColor(m_grid_line_primary_color); }
      else { m_grid_line.setFillColor(m_grid_line_secondary_color); }

      render->draw(m_grid_line);
    }
}

void GraphicalGrid::displayBlocs(Render* render)
{
  for(size_t i=0; i<m_level->getSize().y; i++)
    {
      for(size_t j=0; j<m_level->getSize().x; j++)
	{
	  m_grid_element.setPosition({static_cast<float>(j) * m_bloc_width + m_area.left
		, static_cast<float>(i) * m_bloc_height + m_area.top});

	  // Real display for user
	  switch( m_level->getUsr(i, j) )
	    {
	    case GridValue::Empty :
	      m_grid_element.setFillColor(sf::Color::White);
	      break;
	    case GridValue::Cross :
	      m_grid_element.setFillColor(sf::Color::Blue);
	      break;
	    case GridValue::Block :
	      m_grid_element.setFillColor(sf::Color::Black);
	      break;
	    }

	  // Render with color when the level
	  // is complete.
	  if( m_level->isComplete() )
	    {
	      m_grid_element.setFillColor( m_level->getColor(i, j) );
	      m_drawLines = false;
	    }
	  else
	    {
	      m_drawLines = true;
	    }
	  
	  render->draw(m_grid_element);
	}
    }
}

void GraphicalGrid::display(Render* render)
{
  render->draw(m_grid_bg);
  displayBlocs(render);
  if(m_drawLines) { displayLines(render); }
  
  for(std::unique_ptr<Cursor>& cursor : m_cursors)
    {
      cursor->display(render);
    }

  for(std::unique_ptr<VisualHelper>& helper : m_visual_helpers)
    {
      helper->display(render);
    }
}

GraphicalGrid::~GraphicalGrid()
{
  
}
