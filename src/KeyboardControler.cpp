/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <KeyboardControler.hpp>

KeyboardControler::KeyboardControler()
{
  
}

KeyboardControler::KeyboardControler(std::vector<sf::Keyboard::Key> ctrls)
{
  m_up = ctrls[0];
  m_down = ctrls[1];
  m_left = ctrls[2];
  m_right = ctrls[3];
  m_accept = ctrls[4];
  m_cancel = ctrls[5];
  m_alternative = ctrls[6];
}

/*virtual*/ bool KeyboardControler::up()
{
  return sf::Keyboard::isKeyPressed(m_up);
}

/*virtual*/ bool KeyboardControler::down()
{
  return sf::Keyboard::isKeyPressed(m_down);
}

/*virtual*/ bool KeyboardControler::left()
{
  return sf::Keyboard::isKeyPressed(m_left);
}

/*virtual*/ bool KeyboardControler::right()
{
  return sf::Keyboard::isKeyPressed(m_right);
}

/*virtual*/ bool KeyboardControler::accept()
{
  return sf::Keyboard::isKeyPressed(m_accept);
}

/*virtual*/ bool KeyboardControler::cancel()
{
  return sf::Keyboard::isKeyPressed(m_cancel);
}

/*virtual*/ bool KeyboardControler::alternative()
{
  return sf::Keyboard::isKeyPressed(m_alternative);
}

KeyboardControler::~KeyboardControler()
{
  
}
