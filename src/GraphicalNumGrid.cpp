/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <GraphicalNumGrid.hpp>
#include <GraphicalGrid.hpp>
#include <Render.hpp>
#include <Level.hpp>
#include <Config.hpp>

GraphicalNumGrid::GraphicalNumGrid(Level* level, sf::FloatRect rect)
  : m_level		( level )
  , m_area		( rect )
  , m_grid		( std::make_unique<GraphicalGrid>(m_level, rect) )
  , m_number_x_offset	( m_grid->getBlocWidth()/2.0 )
  , m_number_y_offset	( m_grid->getBlocHeight()/2.0 ) 
{  
  m_number_font.loadFromFile(DATA_DIR "fonts/cabin.ttf");
  m_number.setFont(m_number_font);
  m_number.setCharacterSize( 0.7 * std::min(m_grid->getBlocWidth(), m_grid->getBlocHeight()) );
  m_number.setFillColor(sf::Color::Black);
}

void GraphicalNumGrid::update()
{
  m_grid->update();
}

void GraphicalNumGrid::displayNumbers(Render* render)
{
  displayXNumbers(render);
  displayYNumbers(render);
}

void GraphicalNumGrid::displayYNumbers(Render* render)
{
  m_number.setPosition({m_area.left - m_number_x_offset
	, m_area.top + (m_grid->getBlocHeight() - m_number.getCharacterSize())/2.0f});
  
  for(size_t i=0; i<m_level->getSize().y; i++)
    {
      std::vector<unsigned int> vec_num = m_level->getYNumbers(i);
      std::reverse(vec_num.begin(), vec_num.end());
      
      // position of the first number of the row
      m_number.setPosition({(m_area.left - m_number.getGlobalBounds().width)
	    , m_number.getPosition().y});            

      for(unsigned int const num : vec_num)
	{
	  m_number.setString( std::to_string(num) );
	  m_number.move({-m_number.getGlobalBounds().width - m_number_x_offset, 0});
	  render->draw(m_number);
	}

      m_number.move({0, m_grid->getBlocHeight()});
    }
}

void GraphicalNumGrid::displayXNumbers(Render* render)
{
  m_number.setPosition({m_area.left + (m_grid->getBlocWidth() - m_number.getCharacterSize())/2.0f
	, m_area.top - m_number_y_offset});
  
  for(size_t i=0; i<m_level->getSize().x; i++)
    {
      std::vector<unsigned int> vec_num = m_level->getXNumbers(i);
      std::reverse(vec_num.begin(), vec_num.end());
      
      // position of the first number of the columns
      m_number.setPosition({m_number.getPosition().x
	    , (m_area.top - m_number.getGlobalBounds().height) });

      for(unsigned int const num : vec_num)
	{
	  m_number.setString( std::to_string(num) );
	  m_number.move({0, -m_number.getGlobalBounds().height - m_number_y_offset});
	  render->draw(m_number);
	}

      m_number.move({m_grid->getBlocWidth(), 0});
    }
}

void GraphicalNumGrid::display(Render* render)
{
  m_grid->display(render);
  displayNumbers(render);
}


GraphicalNumGrid::~GraphicalNumGrid()
{
  
}
