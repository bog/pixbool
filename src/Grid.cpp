/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <Grid.hpp>

Grid::Grid(unsigned int width, unsigned int height)
  : m_width(width)
  , m_height(height)
  , m_grid(nullptr)
{
  m_grid = new GridValue*[m_height];
  
  for(size_t i=0; i<m_height; i++)
    {
      m_grid[i] = new GridValue[m_width];
      
      for(size_t j=0; j<m_width; j++)
	{
	  m_grid[i][j] = GridValue::Empty;
	}      
    }
}

void Grid::dump()
{
  for(size_t i=0; i<m_height; i++)
    {
      for(size_t j=0; j<m_width; j++)
	{
	  switch(m_grid[i][j])
	    {
	    case GridValue::Empty:
	      std::cout<< " ";
	      break;
	    case GridValue::Cross:
	      std::cout<< "X";
	      break;
	    case GridValue::Block:
	      std::cout<< "#";
	      break;
	    default:break;
	    }
	}
      
      std::cout<<std::endl;
    }
}

void Grid::loadFromImage(sf::Image image)
{
  if(m_width != image.getSize().x
     || m_height != image.getSize().y)
    {
      throw std::logic_error("Can't use a "
			     + std::to_string(image.getSize().x)
			     + "x"
			     + std::to_string(image.getSize().y)
			     + " image with a "
			     + std::to_string(m_width)
			     + "x"
			     + std::to_string(m_height)
			     + " grid.");
    }
     
  for(size_t i=0; i<m_height; i++)
    {
      for(size_t j=0; j<m_width; j++)
	{
	  sf::Color pixel_color = image.getPixel(j, i);
	  
	  if(pixel_color == sf::Color::White)
	    {
	      m_grid[i][j] = GridValue::Empty;
	    }
	  else
	    {
	      m_grid[i][j] = GridValue::Block;
	    }
	}
    }
}

Grid::~Grid()
{
  for(size_t i=0; i<m_height; i++)
    {
      delete m_grid[i];
    }
  
  delete m_grid;
  m_grid = nullptr;
}
