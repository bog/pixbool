/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <LevelScene.hpp>
#include <Config.hpp>
#include <Grid.hpp>
#include <GraphicalNumGrid.hpp>
#include <GraphicalGrid.hpp>
#include <Cursor.hpp>
#include <Level.hpp>

LevelScene::LevelScene(Core* core)
  : Scene(core)
  , m_inputs_delay(96)
{
  m_level = std::make_unique<Level>(DATA_DIR "grids/chat_3.png");

  float screen_w = static_cast<float>(m_core->getSize().x);
  float screen_h = static_cast<float>(m_core->getSize().y);
  float screen_min = 0.90 * std::min(screen_w, screen_h);
  float length = 5.0 * screen_min / 6.0;

  sf::FloatRect rect( (screen_w - length)/2.0
		      , screen_h - length
		     , length
		     , length);
    
  m_grid = std::make_unique<GraphicalNumGrid>(m_level.get(), rect);

  m_grid->getInnerGrid()->addCursor(sf::Color::Green);
  m_grid->getInnerGrid()->addCursor(sf::Color::Red);
  
  m_inputs_clocks = std::vector<sf::Clock>(2);
}

Cursor* LevelScene::getCursor(size_t cursor)
{
  return m_grid->getInnerGrid()->getCursor(cursor);
}

void LevelScene::updateInput(size_t player)
{
  try
    {
      if( m_inputs_clocks[player].getElapsedTime().asMilliseconds() >= m_inputs_delay )
	{
	  // moving
	  if( m_core->getControler(player)->up() )
	    {
	      m_grid->getInnerGrid()->cursorMove(player, {0, -1});
	    }
	  if( m_core->getControler(player)->down() )
	    {
	      m_grid->getInnerGrid()->cursorMove(player, {0, 1});
	    }
	  if( m_core->getControler(player)->left() )
	    {
	      m_grid->getInnerGrid()->cursorMove(player, {-1, 0});
	    }
	  if( m_core->getControler(player)->right() )
	    {
	      m_grid->getInnerGrid()->cursorMove(player, {1, 0});
	    }
	  m_inputs_clocks[player].restart();
	}
	
      if( m_core->getControler(player)->accept() )
	{
	  m_grid->getInnerGrid()->cursorMark(player);
	}
      if( m_core->getControler(player)->cancel() )
	{
	  m_grid->getInnerGrid()->cursorClean(player);
	}
      if( m_core->getControler(player)->alternative() )
	{
	  m_grid->getInnerGrid()->cursorCross(player);
	}
    }
  catch(std::exception const& e)
    {
      std::cerr<< e.what() <<std::endl;
    }
}

void LevelScene::update()
{
  m_grid->update();
  updateInput(0);
  updateInput(1);
}

void LevelScene::display(Render* render)
{
  m_grid->display(render);
}

LevelScene::~LevelScene()
{
  
}
