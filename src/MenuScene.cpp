/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <MenuScene.hpp>

MenuScene::MenuScene(Core* core)
  : Scene(core)
{

  m_title_font.loadFromFile(DATA_DIR "fonts/cabin.ttf");
  
  m_title.setFont(m_title_font);
  m_title.setFillColor(sf::Color(100, 255, 100));
  m_title.setString(PROJECT_NAME " v" PROJECT_VERSION);
  m_title.setCharacterSize(64);

  {
    float x = (m_core->getSize().x - m_title.getLocalBounds().width)/2;
    float y = (m_core->getSize().y - m_title.getLocalBounds().height)/2;
    m_title.setPosition({x, y});
  
  }

  m_sub_title = sf::Text(m_title);
  m_sub_title.setCharacterSize(m_sub_title.getCharacterSize()/2);
  m_sub_title.setString("press <ENTER> to play");

  {
    float x = (m_core->getSize().x - m_sub_title.getLocalBounds().width)/2;
    float y = (m_core->getSize().y - m_sub_title.getLocalBounds().height)/2;
    m_sub_title.setPosition({x, y});
  
  }
  
  m_sub_title.setFillColor(sf::Color::Blue);
  
  m_sub_title.move({0
	, 2 * m_title.getLocalBounds().height});

}

void MenuScene::update()
{
  if( m_core->getControler(0)->accept() )
    {
      m_core->loadNoScene();
    }
}

void MenuScene::display(Render* render)
{
  render->draw(m_title);
  render->draw(m_sub_title);
}

MenuScene::~MenuScene()
{
  
}
