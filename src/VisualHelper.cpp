/* Copyright (C) 2016 bog

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <VisualHelper.hpp>
#include <GraphicalGrid.hpp>
#include <Cursor.hpp>
#include <Render.hpp>

VisualHelper::VisualHelper(GraphicalGrid* grid, Cursor* cursor)
  : m_grid	(grid)
  , m_cursor	(cursor)
  , m_opacity	(50)
{
  m_color.r = m_cursor->getColor().r;
  m_color.g = m_cursor->getColor().g;
  m_color.b = m_cursor->getColor().b;
  m_color.a = m_opacity;
  
  m_horizontal.setSize({m_grid->getArea().left, m_grid->getBlocHeight()});  
  m_horizontal.setFillColor( m_color );

  m_vertical.setSize({m_grid->getBlocWidth(), m_grid->getArea().top});  
  m_vertical.setFillColor( m_color );
}

void VisualHelper::update()
{
  m_horizontal.setPosition({0
	, m_grid->getArea().top + m_grid->getCursorPos(m_cursor).y
	* m_grid->getBlocHeight()});

  m_vertical.setPosition({m_grid->getArea().left + m_grid->getCursorPos(m_cursor).x
	* m_grid->getBlocWidth()
	, 0});
}

void VisualHelper::display(Render* render)
{
  render->draw(m_horizontal);
  render->draw(m_vertical);
}



VisualHelper::~VisualHelper()
{
  
}
