cmake_minimum_required(VERSION 3.5)

project(pixbool)

file(GLOB
  sources
  src/*.cpp
  include/*.hpp)

file(GLOB_RECURSE
  assets
  share/*)

add_definitions("-Wall -Werror -std=c++17")

include_directories("${PROJECT_SOURCE_DIR}/include")

configure_file("${PROJECT_SOURCE_DIR}/include/Config.hpp.in"
  "${PROJECT_SOURCE_DIR}/include/Config.hpp")

add_executable(pixbool
  "${sources}")

find_package(PkgConfig)
pkg_check_modules(sfml sfml-graphics sfml-audio)

target_link_libraries(pixbool
  ${sfml_LDFLAGS})
